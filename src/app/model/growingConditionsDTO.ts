export class GrowingConditionsDTO {
  id?: string;
  flora?: string;

  soilMoistureMin: number;
  soilMoistureMax: number;

  airHumidityMin: number;
  airHumidityMax: number;

  airTemperatureMin: number;
  airTemperatureMax: number;

  lightIntensityMin: number;
  lightIntensityMax: number;
}
