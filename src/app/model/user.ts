export class AppUserCreateDTO {
    email: string;
    password: string;
}

export class AppUserDTO {
    id: string;
    email: string;
    role: string;
}

export class CurrentUser {
    id: string;
    email: string;
    role: string;
    password: string;

    constructor (
        id: string,
        email: string,
        password: string,
        role: string) {
            this.id = id;
            this.email = email;
            this.password = password;
            this.role = role;
        }
}