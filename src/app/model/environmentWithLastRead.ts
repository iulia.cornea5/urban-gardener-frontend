import { DeviceRead } from "./deviceRead";
import { EnvironmentDTO } from "./environmentDTO";


export class EnvironmentWithLastRead {

    environment: EnvironmentDTO;
    lastRead: DeviceRead;
}