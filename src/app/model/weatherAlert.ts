export class WeatherAlert {
    id: string;
    appUserId: string;
    environmentId: string;
    growingConditionsId: string;
    alertType: string;
    impactDate: Date;
    registrationDate: Date;
    status: String;
}
