import { EnvironmentDTO } from '../model/environmentDTO';
import { DeviceRead } from '../model/deviceRead';

export class EnvironmentWithReads {
    environment: EnvironmentDTO;
    reads: DeviceRead[] = []
}