import {GrowingConditionsDTO} from 'src/app/model/growingConditionsDTO';


export class EnvironmentDTO {
    id?: string;
    name: string;
    orientation?: string;
    outdoor?: boolean;
    latitude?: number;
    longitude?: number;
    growingConditions?: GrowingConditionsDTO[];
}