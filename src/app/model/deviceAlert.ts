export class DeviceAlert {

    id: string;
    // deviceId: string;
    alertType: string;
    registrationDate: Date;
    lastUpdateDate: Date;
    threshold: number;
    lastRegisteredValue: number;
    status: string
    
}