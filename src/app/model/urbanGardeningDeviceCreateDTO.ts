export class UrbanGardeningDeviceCreateDTO {

    id: string;
    name: string;
    currentEnvironmentId: string;
    appUserId: string;
}