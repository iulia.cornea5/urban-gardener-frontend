export class EnvironmentCreateDTO {
    id?: string;
    name: string;
    orientation?: string;
    outdoor?: boolean;
    latitude: number;
    longitude: number;
    growingConditionsId: string;
    appUserId: string;
}