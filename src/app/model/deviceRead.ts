export class DeviceRead{
    
    airTemperatureCelsius: number;
    airHumidityPercentage : number;
    soilMoisturePercentage: number;
    lightIntensityLux: number;
    receivedAt: Date;
}