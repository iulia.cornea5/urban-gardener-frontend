export class Device {
    
    id: string;
    name: string;
    appUserId: string;
    currentEnvironmentId: string;
}