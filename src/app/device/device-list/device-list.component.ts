import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UrbanGardeningDeviceService } from 'src/app/services/urban-gardening-device.service';
import { UserService } from 'src/app/services/user.service';
import { Device } from '../../model/device';
@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {

  devices: Device[] = [];
  displayedColumns=["id", "name", "currentEnvironmentId", "actions"];

  constructor(
    private deviceService: UrbanGardeningDeviceService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {

    this.deviceService.getAllOfUser(this.userService.getCurrentUser().id)
      .subscribe(response => {
        console.log(response)
        this.devices = response;
      })
  }

  edit(device: Device) {
    this.router.navigate(['my-devices/edit'], {state: {editDevice: device}});
  }

  delete(device: Device) {
    
  }

}
