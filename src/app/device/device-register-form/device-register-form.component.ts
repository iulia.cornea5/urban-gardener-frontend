import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UrbanGardeningDeviceCreateDTO } from '../../model/urbanGardeningDeviceCreateDTO'
import { UrbanGardeningDeviceService } from '../../services/urban-gardening-device.service'
import { EnvironmentService } from '../../services/environment.service';
import { EnvironmentDTO } from '../../model/environmentDTO';
import { UserService } from 'src/app/services/user.service';
import { EnvironmentWithLastRead } from 'src/app/model/environmentWithLastRead';
import { ActivatedRoute, Router } from '@angular/router';
import { Device } from 'src/app/model/device';

@Component({
  templateUrl: './device-register-form.component.html',
  selector: 'app-device-register-form',
  styleUrls: ['./device-register-form.component.css']
})
export class DeviceRegisterFormComponent implements OnInit {

  editDevice: Device = null;

  deviceIdFC = new FormControl();
  nameFC = new FormControl();
  environmentFC = new FormControl();

  environments: EnvironmentWithLastRead[] = []

  constructor(
    private environmentService: EnvironmentService,
    private deviceService: UrbanGardeningDeviceService,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.environmentService
      .getAllOfUser(this.userService.getCurrentUser().id)
      .subscribe(response => {
        this.environments = response;
      });
    if (this.activatedRoute.snapshot.url[1].path == 'edit') {
      const device = history.state.editDevice;
      if (device != null) {
        this.prefillFormOnEdit(device);
      } else {
        this.router.navigate(['my-devices/new']);
      }
    }
  }

  prefillFormOnEdit(device: Device) {
    this.editDevice = device
    this.deviceIdFC.setValue(this.editDevice.id);
    this.deviceIdFC.disable();
    this.nameFC.setValue(this.editDevice.name);
    this.environmentFC.setValue(this.editDevice.currentEnvironmentId);
  }

  onRegister() {
    const device = {
      id: this.deviceIdFC.value,
      name: this.nameFC.value,
      currentEnvironmentId: this.environmentFC.value,
      appUserId: this.userService.getCurrentUser().id
    };
    this.deviceService.registerDevice(device).subscribe(response => {
      this.router.navigate(['/my-devices']);
    })
  }

  onUpdate() {
    this.editDevice.name = this.nameFC.value;
    this.editDevice.currentEnvironmentId = this.environmentFC.value;
    this.deviceService.update(this.editDevice).subscribe( reesponse => this.router.navigate(['/my-devices']));
  }

}
