import { Component, Input, OnInit } from '@angular/core';
import { Device } from 'src/app/model/device';

@Component({
  selector: 'app-device-page',
  templateUrl: './device-page.component.html',
  styleUrls: ['./device-page.component.css']
})
export class DevicePageComponent implements OnInit {


  @Input()
  device: Device;

  constructor() { }

  ngOnInit(): void {
  }

}
