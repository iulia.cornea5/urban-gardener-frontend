import { Component, OnInit } from '@angular/core';
import { FormControl, RequiredValidator, Validators } from '@angular/forms';
import { GrowingConditionsService } from '../../services/growing-conditions.service'
import { GrowingConditionsDTO } from '../../model/growingConditionsDTO'
import { EnvironmentCreateDTO } from '../../model/environmentCreateDTO';
import { EnvironmentService } from '../../services/environment.service'
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-environment-form',
  templateUrl: './environment-form.component.html',
  styleUrls: ['./environment-form.component.css']
})
export class EnvironmentFormComponent implements OnInit {

  growingConditionsOptions: GrowingConditionsDTO[] = [];

  constructor(
    private environmentService: EnvironmentService,
    private growingConditionsService: GrowingConditionsService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.growingConditionsService.getGrowingConditions()
      .subscribe(response => {
        console.log(response);
        this.growingConditionsOptions = response
      });
  }

  environmentName = new FormControl('', Validators.required);
  environmentOutdoor = new FormControl();
  environmentOrientation = new FormControl();
  environmentLatitude = new FormControl();
  environmentLongitude = new FormControl();
  environmentGrowingConditions = new FormControl();

  onSave() {
    const env: EnvironmentCreateDTO = this.createObjectFromForm();
    console.log(env);
    this.environmentService.create(env).subscribe(response => {
      // alert(response);
      this.router.navigate(['/my-environments']);

    })
  }

  createObjectFromForm(): EnvironmentCreateDTO {
    return {
      name: this.environmentName.value,
      growingConditionsId: this.environmentGrowingConditions.value,
      outdoor: this.environmentOutdoor.value,
      orientation: this.environmentOrientation.value,
      latitude: this.environmentLatitude.value,
      longitude: this.environmentLongitude.value,
      appUserId: this.userService.getCurrentUser().id
    };
  }

}
