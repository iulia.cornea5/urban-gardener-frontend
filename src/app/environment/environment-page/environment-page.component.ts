import { coerceArray } from '@angular/cdk/coercion';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EnvironmentWithReads } from 'src/app/model/environmentWithReads';
import { EnvironmentDTO } from 'src/app/model/environmentDTO';
import { EnvironmentService } from 'src/app/services/environment.service';
import { ReadingsService } from 'src/app/services/readings.service';

@Component({
  selector: 'app-environment-page',
  templateUrl: './environment-page.component.html',
  styleUrls: ['./environment-page.component.css']
})
export class EnvironmentPageComponent implements OnInit {

  environmentId: string;
  environmentWithReads: EnvironmentWithReads = {
    environment: null,
    reads: []
  };

  readingsColumns=["time", "temperature", "humidity", "soilMoisture", "luminosity"];

  constructor(
    private activatedRoute: ActivatedRoute,
    private environmentService: EnvironmentService,
    private readingsService: ReadingsService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(paramsMap => {
      this.environmentId = paramsMap["id"];

      this.environmentService.get(this.environmentId).subscribe(env => 
        this.environmentWithReads.environment = env);

      this.readingsService.getAll(this.environmentId).subscribe(readings =>
        this.environmentWithReads.reads = readings);

    });
  }

}
