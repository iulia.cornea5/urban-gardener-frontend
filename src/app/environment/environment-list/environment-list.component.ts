import { Component, OnInit } from '@angular/core';
import { EnvironmentWithLastRead } from 'src/app/model/environmentWithLastRead';
import { EnvironmentService } from 'src/app/services/environment.service';
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-environment-list',
  templateUrl: './environment-list.component.html',
  styleUrls: ['./environment-list.component.css']
})
export class EnvironmentListComponent implements OnInit {

  environments: EnvironmentWithLastRead[] = [];

  constructor(
    private userService: UserService,
    private environmentService: EnvironmentService) { }

  ngOnInit(): void {
    console.log(this.userService.getCurrentUser());
    console.log(this.userService.getCurrentUser().id);
    this.environmentService.getAllOfUser(this.userService.getCurrentUser().id)
      .subscribe(
        response => {
          console.log(response);
          this.environments = response;
        },
        error => {
          console.log("am avut eroare")
          
        });
  }

}
