import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { EnvironmentWithLastRead} from '../../model/environmentWithLastRead';

@Component({
  selector: 'app-environment-card',
  templateUrl: './environment-card.component.html',
  styleUrls: ['./environment-card.component.css']
})
export class EnvironmentCardComponent implements OnInit {

  @Input()
  environment: EnvironmentWithLastRead

  constructor() { }

  ngOnInit(): void {
  }

  getLinkToPage(): string {
    return "['environment/'" + this.environment.environment.id + " ]";
  }

}
