import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EnvironmentFormComponent } from './environment/environment-form/environment-form.component';

import { MatDividerModule } from '@angular/material/divider';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';



import {MatFormFieldModule} from '@angular/material/form-field';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { DeviceRegisterFormComponent } from './device/device-register-form/device-register-form.component';
import { HeaderComponent } from './header/header.component';
import { EnvironmentCardComponent } from './environment/environment-card/environment-card.component';
import { EnvironmentPageComponent } from './environment/environment-page/environment-page.component';
import { DevicePageComponent } from './device/device-page/device-page.component';
import { DeviceCardComponent } from './device/device-card/device-card.component';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { EnvironmentListComponent } from './environment/environment-list/environment-list.component';
import { UserRegistrationFormComponent } from './user-registration-form/user-registration-form.component';
import { BasicAuthInterceptor} from "./interceptors/basicAuthInterceptor";
import { DeviceAlertComponent } from './alert/device-alert/device-alert.component';
import {MatTableModule} from '@angular/material/table';
import { GrowingConditionsFormComponent } from './growing-conditions/growing-conditions-form/growing-conditions-form.component';
import { GrowingConditionsListComponent } from './growing-conditions/growing-conditions-list/growing-conditions-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EnvironmentFormComponent,
    DeviceRegisterFormComponent,
    HeaderComponent,
    EnvironmentCardComponent,
    EnvironmentPageComponent,
    DevicePageComponent,
    DeviceCardComponent,
    DeviceListComponent,
    EnvironmentListComponent,
    UserRegistrationFormComponent,
    DeviceAlertComponent,
    GrowingConditionsFormComponent,
    GrowingConditionsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDividerModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatMenuModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatTableModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
