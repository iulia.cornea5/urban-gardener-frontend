import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GrowingConditionsDTO } from 'src/app/model/growingConditionsDTO';
import { GrowingConditionsService } from 'src/app/services/growing-conditions.service';

@Component({
  selector: 'app-growing-conditions-list',
  templateUrl: './growing-conditions-list.component.html',
  styleUrls: ['./growing-conditions-list.component.css']
})
export class GrowingConditionsListComponent implements OnInit {

  displayedColumns=["flora", "airTemperature", "airHumidity", "soilMoisture", "lightIntensity", "actions"];
  conditions: GrowingConditionsDTO[] = [];

  constructor(private growingConditionsService: GrowingConditionsService,
    private router: Router) { }

  ngOnInit(): void {
    this.growingConditionsService.getGrowingConditions().subscribe(
      response => {
        this.conditions = response;
      }
    );
  }

  edit(cond: GrowingConditionsDTO) {
    this.router.navigate(['/growing-conditions/edit'], {state: {editConditions: cond}});
  }

}
