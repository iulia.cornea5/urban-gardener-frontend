import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { GrowingConditionsDTO } from 'src/app/model/growingConditionsDTO';
import { GrowingConditionsService } from 'src/app/services/growing-conditions.service';

@Component({
  selector: 'app-growing-conditions-form',
  templateUrl: './growing-conditions-form.component.html',
  styleUrls: ['./growing-conditions-form.component.css']
})
export class GrowingConditionsFormComponent implements OnInit {

  editConditions: GrowingConditionsDTO = null;

  flora = new FormControl('', Validators.required);

  airTemperatureMin = new FormControl();
  airTemperatureMax = new FormControl();

  airHumidityMin = new FormControl();
  airHumidityMax = new FormControl();

  soilMoistureMin = new FormControl();
  soilMoistureMax = new FormControl();

  lightIntensityMin = new FormControl();
  lightIntensityMax = new FormControl();


  constructor(private growingConditionsService: GrowingConditionsService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.url[1].path == 'edit') {
      const cond = history.state.editConditions;
      if (!!cond) {
        this.prefillFormOnEdit(cond);
      } else {
        this.router.navigate(['/growing-conditions/new']);
      }
    }
  }

  prefillFormOnEdit(cond: GrowingConditionsDTO) {
    this.editConditions = cond;
    this.flora.setValue(this.editConditions?.flora);
    this.airTemperatureMin.setValue(this.editConditions?.airTemperatureMin);
    this.airTemperatureMax.setValue(this.editConditions?.airTemperatureMax);
    this.airHumidityMin.setValue(this.editConditions?.airHumidityMin);
    this.airHumidityMax.setValue(this.editConditions?.airHumidityMax);
    this.soilMoistureMin.setValue(this.editConditions?.soilMoistureMin);
    this.soilMoistureMax.setValue(this.editConditions?.soilMoistureMax);
    this.lightIntensityMin.setValue(this.editConditions?.lightIntensityMin);
    this.lightIntensityMax.setValue(this.editConditions?.lightIntensityMax);
  }

  create() {
    const conditions: GrowingConditionsDTO = {
      id: null,
      flora: this.flora.value,
      airTemperatureMin: this.airTemperatureMin.value,
      airTemperatureMax: this.airTemperatureMax.value,
      airHumidityMin: this.airHumidityMin.value,
      airHumidityMax: this.airHumidityMax.value,
      soilMoistureMin: this.soilMoistureMin.value,
      soilMoistureMax: this.soilMoistureMax.value,
      lightIntensityMin: this.lightIntensityMin.value,
      lightIntensityMax: this.lightIntensityMax.value
    }

    this.growingConditionsService.create(conditions).subscribe(response => {
      this.router.navigate(['/growing-conditions']);
    });
  }

  update() {
    this.editConditions.flora = this.flora.value;
    this.editConditions.airHumidityMax = this.airHumidityMax.value;
    this.editConditions.airHumidityMin = this.airHumidityMin.value;
    this.editConditions.airTemperatureMax = this.airTemperatureMax.value;
    this.editConditions.airTemperatureMin = this.airTemperatureMin.value;
    this.editConditions.soilMoistureMax = this.soilMoistureMax.value;
    this.editConditions.soilMoistureMin = this.soilMoistureMin.value;
    this.editConditions.lightIntensityMax = this.lightIntensityMax.value;
    this.editConditions.lightIntensityMin = this.lightIntensityMin.value;

    this.growingConditionsService.update(this.editConditions.id, this.editConditions).subscribe(
      response => {
        history.pushState({ editConditions: null }, '', '');
        this.router.navigate(['/growing-conditions']);
      }
    );
  }

}
