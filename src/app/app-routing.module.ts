import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EnvironmentFormComponent } from './environment/environment-form/environment-form.component';
import { DeviceRegisterFormComponent } from './device/device-register-form/device-register-form.component';
import { EnvironmentListComponent } from './environment/environment-list/environment-list.component';
import { EnvironmentPageComponent } from './environment/environment-page/environment-page.component';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { UserRegistrationFormComponent } from './user-registration-form/user-registration-form.component';
import { GrowingConditionsFormComponent } from './growing-conditions/growing-conditions-form/growing-conditions-form.component';
import { GrowingConditionsListComponent } from './growing-conditions/growing-conditions-list/growing-conditions-list.component';


const routes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "my-environments", component: EnvironmentListComponent},
  { path: "environments/new", component: EnvironmentFormComponent},
  { path: "my-devices", component: DeviceListComponent},
  { path: "my-devices/new", component: DeviceRegisterFormComponent},
  { path: "my-devices/edit", component: DeviceRegisterFormComponent},
  { path: "environment/:id", component: EnvironmentPageComponent},
  { path: "login", component: UserRegistrationFormComponent},
  { path: "growing-conditions", component: GrowingConditionsListComponent},
  { path: "growing-conditions/new", component: GrowingConditionsFormComponent},
  { path: "growing-conditions/edit", component: GrowingConditionsFormComponent},
  { path: "**", redirectTo: "home"}

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
