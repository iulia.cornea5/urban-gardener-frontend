import { Component, Input, OnInit } from '@angular/core';
import { DeviceAlert} from '../../model/deviceAlert';
import { DeviceAlertService } from '../../services/device-alert.service';


@Component({
  selector: 'app-device-alert',
  templateUrl: './device-alert.component.html',
  styleUrls: ['./device-alert.component.css']
})
export class DeviceAlertComponent implements OnInit {

  alerts: DeviceAlert[] = []

  alertsColumns=["icon", "alertType", "lastRegisteredValue", "threshold", "registrationDate", "lastUpdateDate", "acknowledge"];

  @Input()
  environmentId: string;

  constructor(private deviceAlertService: DeviceAlertService) { }

  ngOnInit(): void {
    this.deviceAlertService.getAllActiveOfEnvironment(this.environmentId).subscribe(
      response => this.alerts = response);
  }

  acknowledgeAlert(alert: DeviceAlert) {
    this.deviceAlertService.acknowledgeAlert(alert.id).subscribe(response => {
      this.ngOnInit();
    })
  }

  prettifyAlertType(alertType: string):string {
    return alertType.replace(new RegExp('_', 'g'), ' ').toLowerCase();
  }

}
