import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  userLoggedIn(): boolean {
    return !!this.userService.getCurrentUser();
  }

  logout() {
    this.userService.logout();
  }

  username(){
    return this.userService.getCurrentUser()?.email;
  }

  userIsAdmin(): boolean {
    return this.userService.getCurrentUser()?.role == 'ADMIN';
  }
  
  userIsRegularUser(): boolean {
    return this.userService.getCurrentUser()?.role == 'USER';
  }

}
