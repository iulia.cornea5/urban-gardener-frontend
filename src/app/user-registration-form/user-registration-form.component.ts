import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EnvironmentService } from '../services/environment.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.css']
})
export class UserRegistrationFormComponent implements OnInit {

  emailLoginFC = new FormControl();
  passwordLoginFC = new FormControl();
  
  emailRegisterFC = new FormControl();
  passwordRegisterFC = new FormControl();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  onLogin() {
    const user = {email: this.emailLoginFC.value, password: this.passwordLoginFC.value};
    this.userService.login(user);
  }

  onRegisterAndLogin() {
    const user = {email: this.emailRegisterFC.value, password: this.passwordRegisterFC.value};
    this.userService.signup(user).subscribe(
      createdUser => {
        this.userService.login(user);
      }
    );
  }

}
