import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

    constructor(private userService: UserService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const currentUser = this.userService.getCurrentUser();
        if (!!currentUser) {
            const modifiedReq = req.clone({
                headers: req.headers.set('Authorization', this.getBasicAuth(currentUser.email, currentUser.password)),
            });
            return next.handle(modifiedReq);
        }
        return next.handle(req);
    }

    getBasicAuth(username: string, password: string): string {
        return "Basic " + btoa(username + ":" + password);
    }

}