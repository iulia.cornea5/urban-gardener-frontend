import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class WeatherAlertService {

  constructor(private http: HttpClient) { }

  getAllActiveOfUser(userId: string): Observable<any> {
    return this.http.get('/api/weather-alert/active/user/' + userId);
  }

  getAllOfUser(userId: string): Observable<any> {
    return this.http.get('/api/weather-alert/user/' + userId);
  } 

  getAllOfEnvironment(environmentId: string): Observable<any> {
    return this.http.get('/api/weather-alert/environment/' + environmentId);
  }

  getAllActiveOfEnvironment(environmentId: string): Observable<any> {
    return this.http.get('/api/weather-alert/active/environment/' + environmentId);
  }

  
  acknowledgeAlert(alertId: string): Observable<any> {
    return this.http.post('/api/weather-alert/acknowledge/' + alertId, null);
  }
}
