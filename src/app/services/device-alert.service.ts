import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DeviceAlertService {

  constructor(private http: HttpClient) { }

  getAllActiveOfUser(userId: string): Observable<any> {
    return this.http.get('/api/device-alert/active/user/' + userId);
  }

  getAllOfUser(userId: string): Observable<any> {
    return this.http.get('/api/device-alert/user/' + userId);
  }

  getAllOfEnvironment(environmentId: string): Observable<any> {
    return this.http.get('/api/device-alert/environment/' + environmentId);
  }

  getAllActiveOfEnvironment(environmentId: string): Observable<any> {
    return this.http.get('/api/device-alert/active/environment/' + environmentId);
  }

  acknowledgeAlert(alertId: string): Observable<any> {
    return this.http.post('/api/device-alert/acknowledge/' + alertId, null);
  }
}
