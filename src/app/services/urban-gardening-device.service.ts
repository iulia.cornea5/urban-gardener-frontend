import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrbanGardeningDeviceCreateDTO } from '../model/urbanGardeningDeviceCreateDTO';

@Injectable({
  providedIn: 'root'
})
export class UrbanGardeningDeviceService {

  constructor(private http: HttpClient) { }

  registerDevice(device: UrbanGardeningDeviceCreateDTO): Observable<any> {
    return this.http.post('/api/urban-gardening-device', device)
  }

  update(device: UrbanGardeningDeviceCreateDTO): Observable<any> {
    return this.http.put('/api/urban-gardening-device/' + device.id, device);
  }

  getAllOfUser(userId: string): Observable<any> {
    return this.http.get('/api/urban-gardening-device/user/' + userId);
  }
}
