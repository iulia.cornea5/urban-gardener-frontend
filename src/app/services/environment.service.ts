import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { EnvironmentCreateDTO } from '../model/environmentCreateDTO';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  constructor(private http: HttpClient) { }

  create(createDTO: EnvironmentCreateDTO): Observable<any> {
    return this.http.post('/api/environment', createDTO);
  }

  getAllOfUser(userId: string): Observable<any> {
    return this.http.get('/api/environment/user/' + userId);
  }

  get(environmentId: string):Observable<any> {
    return this.http.get('/api/environment/' + environmentId);
  }

  getAll(): Observable<any> {
    return this.http.get('/api/environment', 
    {
      headers : {"Authorization": "Basic aW9uOnBhc3MxMjM="}
    })
  }

}
