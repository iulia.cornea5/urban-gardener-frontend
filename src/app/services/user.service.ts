import { HttpClient } from '@angular/common/http';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppUserCreateDTO, AppUserDTO, CurrentUser } from '../model/user'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserLabel = "currentUser";

  constructor(
    private httpClient: HttpClient,
    private router: Router) { }

  signup(user: AppUserCreateDTO): Observable<any> {
    return this.httpClient.post("api/user/signup", user);
  }

  login(credentialsUser: AppUserCreateDTO) {
    return this.httpClient.post("api/user/login", credentialsUser).subscribe(
      response => {
        const appUser = response as AppUserDTO;
        localStorage.setItem(this.currentUserLabel, JSON.stringify(new CurrentUser(appUser.id, appUser.email, credentialsUser.password, appUser.role)));
        if (appUser.role == 'USER') {
          this.router.navigate(['/my-environments']);
        }
        if (appUser.role == 'ADMIN') {
          this.router.navigate(['/growing-conditions']);
        }
      },
      error => {
        alert("Invalid credentials");
      }
    );
  }

  logout() {
    localStorage.setItem(this.currentUserLabel, null);
    this.router.navigate(['/home']);
  }

  getAllUsers(): Observable<any> {
    return this.httpClient.get("api/user");
  }

  getUserById(id: string): Observable<any> {
    return this.httpClient.get("api/user/" + id);
  }

  getCurrentUser(): CurrentUser {
    return JSON.parse(localStorage.getItem(this.currentUserLabel));
  }

}
