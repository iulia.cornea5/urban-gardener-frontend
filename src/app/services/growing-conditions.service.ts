import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { GrowingConditionsDTO } from '../model/growingConditionsDTO';

@Injectable({
  providedIn: 'root'
})
export class GrowingConditionsService {

  constructor(private http: HttpClient) {}

  create(conditions: GrowingConditionsDTO): Observable <any> {
    return this.http.post('/api/growing-conditions', conditions);
  }

  update(id: string, conditions: GrowingConditionsDTO): Observable<any> {
    return this.http.put('/api/growing-conditions/' + id, conditions);
  }

  getGrowingConditions(): Observable <any> {
    return this.http.get('/api/growing-conditions');
  }


}
